// define the Schemas for the db

var mongoose = require('mongoose')
  , Schema = mongoose.Schema;
  

// the test data as mapped on remote mongo db
var testData = new Schema({

  item: { name: String, quantity: Number},

}, { collection : 'testdata' }
);

module.exports = mongoose.model('testData', testData);

