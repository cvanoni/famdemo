
var socket = io();

var data = [];


function buy(param){

  console.log("buy ", datareg[param]);
  socket.emit('order-buy', {'item': datareg[param], 'quantity': 1});

}

function sell(param){

  console.log("sell ", datareg[param]);
  socket.emit('order-sell', {'item': datareg[param], 'quantity': 1});

}


var datareg = [];

socket.on('orderlist', function( data ){
    
    console.log("orderlist received", data);

    data.forEach( function (d,idx) {

      datareg.push(d._id);

      var newitem = $('<div id="item">');  

      $('#items')
        .append(newitem); 

        $('#item')
          .append($('<div id="sell-button-' + idx + '" class="small-2 column">'));
        $('#sell-button-' + idx)
            .append($('<button onclick="sell(' + idx + ')" type="button" class="alert button">Sell</button>'));

        $('#item')
        .append($('<div id="item-quantity-' + idx + '" class="small-2 column">'));
        $('#item-quantity-' + idx)
          .append($('<div>').text(d.item.quantity));

        $('#item')
        .append($('<div id="item-name-' + idx + '" class="small-6 column">'));
        $('#item-name-' + idx)
          .append($('<div>').text(d.item.name));

        $('#item')
         .append($('<div id="buy-button-' + idx + '" class="small-2 column">'));
        $('#buy-button-' + idx)
            .append($('<button onclick="buy(' + idx + ')" type="button" class="success button">Buy</button>'));

    });

});


socket.on('update-item', function (record) {

  console.log('update-item', record);

  var idx = datareg.indexOf(record._id);
  $('#item-quantity-' + idx).text(record.item.quantity);


});


socket.on('chat-message', function( msg ){
  $('#messages').append($('<li>').text( msg ));
});


$('form').submit(function(){
  socket.emit('chat-message', $('#send-msg-button').val());
  $('#send-msg-button').val('');
  return false;
});


