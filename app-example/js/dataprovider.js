// define the Schemas for the db

var mongoose = require('mongoose')
  , Schema = mongoose.Schema;
  

// for the data coming from meteo api
var testData = new Schema({
    
    
    //itemname: {type: String},  // location relevant for the prognosis (e.g. LUG, OTL)

    items: {type: Number},
    
    
});


module.exports = mongoose.model('testData', testData);

