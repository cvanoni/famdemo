// setup dependencies and main variables
var express = require('express'); 
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mongoose = require('mongoose');
var assert = require('assert');


// -------------cloud db----------------
// remote db cconnection
var dburl = 'mongodb://famvac:fampas@ds061335.mongolab.com:61335/famdemodb' ;



mongoose.connect(dburl, function (err, res) {
  if (err) {
    console.log ('ERROR connecting to: ' + dburl + '. ' + err);
  } else {
    console.log ('Succeeded connected to: ' + dburl);
  }
});
//  data model
var testData = require('./dataprovider');


// -----------REST routing------------------
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.use('/js', express.static(__dirname + '/js'));

// ------------help functions----------------------

function senddata (socket){

    testData.find({}, function (err,res) {

      // emit the data to the client
      socket.emit('orderlist', res);

      console.log("orderlist emitted");

    })
};


function buy (item_id, quantity) {

  console.log("in buy function:", item_id, quantity);

  testData.find( {_id: item_id}, function (err, record) {
    
    record[0].item.quantity += 1;

    record[0].save(function(err) {
      if (err) throw err;

      console.log("item ", record[0] , " buyed.")
      console.log("Record successfully updated!");

      io.emit('update-item', record[0]);

      console.log("emit updated item");

    });

  });
};

function sell (item_id, quantity) {

  console.log('in sell function:', item_id, quantity); 

  testData.find( {_id: item_id}, function (err, record) {
    
    //console.log("record res", record);

    record[0].item.quantity -= 1;

    record[0].save(function(err) {
      if (err) throw err;

      console.log("item ", record[0] , " selled.")
      console.log("Record successfully updated!");
      
      io.emit('update-item', record[0]);

      console.log("emit updated item");

    });

  });

};


// -----------WEBSOCKET---------------------
io.on('connection', function (socket) {


  // ----------------init-----------------
  console.log("a user connected", socket.id);

  // get the data from the remote db and sent it to the new user
  senddata(socket);

  // ----------------items functionality-----------------

  // on buy order
  socket.on('order-buy', function (payload) {

    console.log("order to buy received:", payload);
    buy(payload['item'], payload['quantity']);    

  });

  // on sell order
  socket.on('order-sell', function (payload) {

    console.log("order to sell received:", payload);
    sell(payload['item'], payload['quantity']);

  });

  // ----------------chat functionality-----------------
  // on chat message
  socket.on('chat message', function (msg) {
    
    console.log("msg received:", msg);    

    io.emit('chat message', msg);
  });



});


// serving
http.listen(3000, function(){
  console.log("listening on *:3000");
});
